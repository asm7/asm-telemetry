# Configuring ASM on GKE multicluster with Prometheus, Grafana, Jaeger and Kiali

```mermaid
flowchart TB
subgraph GCP
  subgraph gke-ingress
    subgraph A[istio-system ns]
      subgraph apps
        amci([apps-mci])
        amcs([apps-mcs])
        abackend([apps-backend])
      end
      subgraph telemetry
        tmci([telemetry-mci])
        tmcs([telemetry-mcs])
        tbackend([telemetry-backend])
      end
    end
  end
  subgraph mesh
    subgraph gke-west
      subgraph B[istio-system ns]
        J[istio-ingressgateway] -.- K(["Gateway(apps)"]) & L(["Gateway(telemetry)"]); 
      end
      subgraph D[telemetry ns]
        M[prometheus]
        N[grafana]
        O[jaeger]
        P[kiali]
      end
      subgraph F[bank-of-anthos ns]
        X[frontend] -.- Y([VirtualService])
      end
      subgraph H[online-boutique ns]
        Z[frontend] -.- AA([VirtualService])
      end
      J ==> F & H
      J ==>|IAP Oauth| D
      F & H -.->|metrics,traces| D
    end
    subgraph gke-central
      subgraph C[istio-system ns]
        U[istio-ingressgateway] -.- V(["Gateway(apps)"]) & W(["Gateway(telemetry)"])
      end
      subgraph E[telemetry ns]
        Q[prometheus]
        R[grafana]
        S[jaeger]
        T[kiali]
      end
      subgraph G[bank-of-anthos ns]
        AB[frontend] -.- AC([VirtualService])
      end
      subgraph I[online-boutique ns]
        AD[frontend] -.- AE([VirtualService])
      end
      U ==> G & I
      U ==>|IAP Oauth| E
      G & I -.->|metrics,traces| E
    end
  end
  apps & telemetry -.->|"configures MCI and MCS"| GCLB
  CloudArmor ==> GCLB ==> J & U
  IAP ==> GCLB
end
  Clients ==> CloudArmor
  InternalUsers ==> IAP

linkStyle 6,15,23,25 stroke:green;
classDef default fill:#c4f5fc;
classDef thickBox stroke-width:4px,fill:#c4f5fc;
classDef noBox stroke-width:0px,fill:#c4f5fc;
classDef dotted fill:#c4f5fc,stroke-dasharray: 5 2;
classDef bluefill fill:#caf0f8;
class gke-ingress,gke-west,gke-central thickBox;
class mesh,A,B,C,D,E,F,G,H,I dotted;
class GCP noBox;
```

This tutorial shows how to configure ASM on multiple clusters with Prometheus, Grafana, Jaeger and Kiali. Prometheus and Grafana are deployed using GCP marketplace and community Helm charts are used to deploy Jaeger and Kiali.  

## Objective

+   Create three GKE clusters. Two clusters are used to deploy applications while the third cluster, `gke-ingress`, is used to configure [MultiClusterIngress (MCI)](https://cloud.google.com/kubernetes-engine/docs/concepts/multi-cluster-ingress).
+   Register all clusters to an [Environ](https://cloud.google.com/anthos/multicluster-management/environs#:~:text=Environs%20are%20a%20Google%20Cloud,cluster%20functionality%20works%20in%20Anthos.) so that you can utilize advanced features.
+   Enable MultiClusterIngress on the `gke-ingress` cluster.
+   Install [Anthos Service Mesh (ASM)](https://cloud.google.com/anthos/service-mesh) on the two app clusters with custom ingress gateway.
+   Deploy and configure OSS telemetry tools (Prometheus, Grafana, Jaeger and Kiali) to both clusters. Prometheus and Grafana are deployed using [GCP marketplace](https://cloud.google.com/marketplace) and Jaeger and Kiali are deployed using community maintained helm charts.
+   Expose all telemetry tools securely using MulticlusterIngress, [Google managed certificates](https://cloud.google.com/kubernetes-engine/docs/how-to/managed-certs) and [Identity Aware Proxy](https://cloud.google.com/iap).
+   Deploy two sample applications - [Bank of Anthos](https://github.com/GoogleCloudPlatform/bank-of-anthos) and [Online Boutique](https://github.com/GoogleCloudPlatform/microservices-demo) on the two app clusters in two different namespaces.
+   Configure DNS names to access the `frontend` service of both applications using [Cloud Endpoints](https://cloud.google.com/endpoints/docs/openapi/cloud-goog-dns-configure) and create Google managed SSL certificates using the Cloud endpoints DNS names.
+   Configure a [Cloud Armor](https://cloud.google.com/armor) security policy and rules.
+   Configure MCI to access Online Boutique and Bank of Anthos applications through both clusters' `istio-ingressgateways` using a single MultiClusterIngress (GCLB frontend). 
+   Use [Gateway](https://istio.io/latest/docs/reference/config/networking/gateway/) and [VirtualService](https://istio.io/latest/docs/reference/config/networking/virtual-service/) resources on both clusters to send traffic to the `frontend` Services for both applications.

## Documentation

This tutorial uses the following documents:

+   [ASM Install](https://cloud.google.com/service-mesh/docs/scripted-install/gke-install)
+   [Multicluster Ingress](https://cloud.google.com/kubernetes-engine/docs/how-to/multi-cluster-ingress)
+   [From edge to mesh](https://cloud.google.com/solutions/exposing-service-mesh-apps-through-gke-ingress#mesh_ingress_gateway)
+   [Service Mesh Ingress](https://istio.io/latest/docs/tasks/traffic-management/ingress/ingress-control/)
+   [ASM and Identity aware proxy](https://cloud.google.com/service-mesh/v1.5/docs/iap-integration)

## Environment

1.  Create a WORKDIR folder.

    ```bash
    mkdir -p asm-telemetry && cd asm-telemetry && export WORKDIR=`pwd`
    ```

1.  Create envars.

    ```bash
    export ORG_ID=YOUR ORG ID
    export ADMIN_USER=YOUR USER EMAIL
    export BILLING_ACCOUNT=YOUR BILLING ACCOUT. YOU MUST BE BILLING ADMIN
    export FOLDER_NAME=YOUR FOLDER NAME HERE
    export PROJECT_NAME=YOUR PROJECT NAME (NOT PROJECT ID) HERE
    cat <<EOF > vars.sh
    export WORKDIR=${WORKDIR}
    export ADMIN_USER=${ADMIN_USER}
    export FOLDER_NAME=${FOLDER_NAME}
    export PROJECT_NAME=${PROJECT_NAME}
    export CLUSTER_1=gke-west
    export CLUSTER_2=gke-central
    export CLUSTER_1_ZONE=us-west2-a
    export CLUSTER_2_ZONE=us-central1-a
    export CLUSTER_INGRESS=gke-ingress
    export CLUSTER_INGRESS_ZONE=us-west1-a
    export ASM_VERSION=1.9
    export ISTIO_VERSION=1.9.1-asm.1
    export ASM_LABEL=asm-191-1
    export GSA_NAME=app-gsa
    export KSA_NAME=bank-ksa
    export BANK_DNS_PREFIX=bank
    export SHOP_DNS_PREFIX=shop
    export KIALI_DNS_PREFIX=kiali
    export PROMETHEUS_DNS_PREFIX=prometheus
    export GRAFANA_DNS_PREFIX=grafana
    export JAEGER_DNS_PREFIX=jaeger
    EOF
    ```

## Tooling

1.  Install [krew](https://krew.sigs.k8s.io/) and plugins used in this guide.

    ```bash
    (
    set -x; cd "$(mktemp -d)" &&
    curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/krew.{tar.gz,yaml}" &&
    tar zxvf krew.tar.gz &&
    KREW=./krew-"$(uname | tr '[:upper:]' '[:lower:]')_amd64" &&
    $KREW install --manifest=krew.yaml --archive=krew.tar.gz &&
    $KREW update
    )
    echo -e "export PATH="${PATH}:${HOME}/.krew/bin"" >> ~/.bashrc && source ~/.bashrc

    kubectl krew install ctx
    kubectl krew install ns
    kubectl krew install neat
    ```

## Setting up your environment

1.  Create a GCP folder and a project in your organization and enable billing. Creating a folder makes it easy to organize GCP resources. At the end, you can delete the project and the folder.

    ```bash
    gcloud alpha resource-manager folders create \
    --display-name=${FOLDER_NAME} \
    --organization=${ORG_ID}

    export FOLDER_ID=$(gcloud resource-manager folders list --organization=${ORG_ID} | grep ${FOLDER_NAME} | awk '{print $3}')
    [ -z "$FOLDER_ID" ] && echo "FOLDER_ID is not exported" || echo "FOLDER_ID is $FOLDER_ID"
    echo -e "export FOLDER_ID=$FOLDER_ID" >> $WORKDIR/vars.sh
    echo -e "export PROJECT_ID=${PROJECT_NAME}-${FOLDER_ID}" >> $WORKDIR/vars.sh
    source ${WORKDIR}/vars.sh

    gcloud projects create ${PROJECT_ID} \
    --folder ${FOLDER_ID}

    gcloud beta billing projects link ${PROJECT_ID} \
    --billing-account ${BILLING_ACCOUNT}

    gcloud config set project ${PROJECT_ID}
    echo -e "export PROJECT_ID=${PROJECT_ID}" >> $WORKDIR/vars.sh
    echo -e "export PROJECT_NUM=$(gcloud projects describe ${PROJECT_ID} --format='value(projectNumber)')" >> $WORKDIR/vars.sh
    source $WORKDIR/vars.sh
    echo -e "export WORKLOAD_POOL=${PROJECT_ID}.svc.id.goog" >> $WORKDIR/vars.sh
    echo -e "export MESH_ID=proj-${PROJECT_NUM}" >> $WORKDIR/vars.sh
    source $WORKDIR/vars.sh
    ```

## Enable APIs

1.  Enable the required APIs

    ```bash
    gcloud services enable \
    --project=${PROJECT_ID} \
    anthos.googleapis.com \
    container.googleapis.com \
    compute.googleapis.com \
    monitoring.googleapis.com \
    logging.googleapis.com \
    cloudtrace.googleapis.com \
    meshca.googleapis.com \
    meshtelemetry.googleapis.com \
    meshconfig.googleapis.com \
    iamcredentials.googleapis.com \
    gkeconnect.googleapis.com \
    gkehub.googleapis.com \
    multiclusteringress.googleapis.com \
    iap.googleapis.com \
    cloudresourcemanager.googleapis.com
    ```

## Create GKE clusters

1.  Create three GKE clusters. Two clusters are used for deploying applications while third cluster, the `gke-ingress` cluster, is used to configure MulticlusterIngress.

    ```bash
    gcloud container clusters create ${CLUSTER_1} \
    --project ${PROJECT_ID} \
    --zone=${CLUSTER_1_ZONE} \
    --machine-type "e2-standard-4" \
    --num-nodes "2" --min-nodes "2" --max-nodes "5" \
    --enable-stackdriver-kubernetes --enable-ip-alias --enable-autoscaling \
    --workload-pool=${WORKLOAD_POOL} \
    --addons=HttpLoadBalancing \
    --verbosity=none \
    --labels=mesh_id=${MESH_ID} --async

    gcloud container clusters create ${CLUSTER_2} \
    --project ${PROJECT_ID} \
    --zone=${CLUSTER_2_ZONE} \
    --machine-type "e2-standard-4" \
    --num-nodes "2" --min-nodes "2" --max-nodes "5" \
    --enable-stackdriver-kubernetes --enable-ip-alias --enable-autoscaling \
    --workload-pool=${WORKLOAD_POOL} \
    --addons=HttpLoadBalancing \
    --verbosity=none \
    --labels=mesh_id=${MESH_ID} --async

    gcloud container clusters create ${CLUSTER_INGRESS} \
    --project ${PROJECT_ID} \
    --zone=${CLUSTER_INGRESS_ZONE} \
    --machine-type "e2-standard-4" \
    --num-nodes "1" --min-nodes "1" --max-nodes "2" \
    --enable-stackdriver-kubernetes --enable-ip-alias --enable-autoscaling \
    --verbosity=none \
    --workload-pool=${WORKLOAD_POOL}
    ```

1.  Confirm clusters are `RUNNING`.

    ```bash
    gcloud container clusters list
    ```

The output is similar to the following:  

    NAME         LOCATION       MASTER_VERSION    MASTER_IP       MACHINE_TYPE   NODE_VERSION      NUM_NODES  STATUS
    gke-central  us-central1-a  1.18.12-gke.1210  34.69.53.228    e2-standard-4  1.18.12-gke.1210  2          RUNNING
    gke-ingress  us-west1-a     1.18.12-gke.1210  35.230.36.119   e2-standard-4  1.18.12-gke.1210  1          RUNNING
    gke-west     us-west2-a     1.18.15-gke.1501  35.235.122.105  e2-standard-4  1.18.15-gke.1501  2          RUNNING

1.  Connect to clusters.

    ```bash
    touch ${WORKDIR}/asm-kubeconfig
    echo -e "export KUBECONFIG=${WORKDIR}/asm-kubeconfig" >> ${WORKDIR}/vars.sh && source vars.sh
    gcloud container clusters get-credentials ${CLUSTER_1} --zone ${CLUSTER_1_ZONE}
    gcloud container clusters get-credentials ${CLUSTER_2} --zone ${CLUSTER_2_ZONE}
    gcloud container clusters get-credentials ${CLUSTER_INGRESS} --zone ${CLUSTER_INGRESS_ZONE}
    ```

> Remember to unset your `KUBECONFIG` var at the end.  

1.  Rename cluster context for easy switching.

    ```bash
    kubectl ctx ${CLUSTER_1}=gke_${PROJECT_ID}_${CLUSTER_1_ZONE}_${CLUSTER_1}
    kubectl ctx ${CLUSTER_2}=gke_${PROJECT_ID}_${CLUSTER_2_ZONE}_${CLUSTER_2}
    kubectl ctx ${CLUSTER_INGRESS}=gke_${PROJECT_ID}_${CLUSTER_INGRESS_ZONE}_${CLUSTER_INGRESS}
    ```

1.  Confirm both cluster contextx are present.

    ```bash
    kubectl ctx
    ```

The output is similar to the following:  

    gke-central
    gke-ingress
    gke-west

## Registering clusters to an Environ

1.  Register all clusters to an [Environ](https://cloud.google.com/anthos/multicluster-management/environs).

    ```bash
    gcloud container hub memberships register ${CLUSTER_INGRESS} \
    --project=${PROJECT_ID} \
    --gke-cluster=${CLUSTER_INGRESS_ZONE}/${CLUSTER_INGRESS} \
    --enable-workload-identity

    gcloud container hub memberships register ${CLUSTER_1} \
    --project=${PROJECT_ID} \
    --gke-cluster=${CLUSTER_1_ZONE}/${CLUSTER_1} \
    --enable-workload-identity

    gcloud container hub memberships register ${CLUSTER_2} \
    --project=${PROJECT_ID} \
    --gke-cluster=${CLUSTER_2_ZONE}/${CLUSTER_2} \
    --enable-workload-identity

    gcloud container hub memberships list
    ```

The output is similar to the following:  

    NAME            EXTERNAL_ID
    gke-west        7fe5b7ce-50d0-4e64-a9af-55d37b3dd3fa
    gke-central     6f1f6bb2-a3f6-4e9c-be52-6907d9d258cd
    gke-ingress     3574ee0f-b7e6-11ea-9787-42010a8a019c

1.  Enable MCI in the CLUSTER_INGRESS

    ```bash
    gcloud alpha container hub ingress enable \
    --config-membership=projects/${PROJECT_ID}/locations/global/memberships/${CLUSTER_INGRESS}

    gcloud alpha container hub ingress describe
    ```

The output is similar to the following:  

    featureState:
      details:
        code: OK
        description: Ready to use
      detailsByMembership:
        projects/217631055577/locations/global/memberships/gke-central:
          code: OK
        projects/217631055577/locations/global/memberships/gke-ingress:
          code: OK
        projects/217631055577/locations/global/memberships/gke-west:
          code: OK
      lifecycleState: ENABLED
    multiclusteringressFeatureSpec:
      configMembership: projects/asm-multi-environment-cluster/locations/global/memberships/gke-ingress
    name: projects/asm-multi-environment-cluster/locations/global/features/multiclusteringress

> It takes a few moments for the controller to configure MCI. You may have to run the describe command a few times. Do not proceed until you see the output shown.  

1.  Verify that the two CRDs (MCS and MCI) have been deployed in the CLUSTER_INGRESS.

    ```bash
    kubectl --context=${CLUSTER_INGRESS} get crd | grep multicluster
    ```

The output is similar to the following:  

    multiclusteringresses.networking.gke.io     2020-10-29T17:32:50Z
    multiclusterservices.networking.gke.io      2020-10-29T17:32:50Z

1.  Create FW rule to allow traffic between Pods in all clusters.

    ```bash
    gcloud compute firewall-rules create all-10 \
    --project ${PROJECT_ID} \
    --network default \
    --allow all \
    --direction INGRESS \
    --source-ranges 10.0.0.0/8
    ```

## Installing ASM

1.  Download the version of the script that installs Anthos Service Mesh to the current working directory.

    ```bash
    curl https://storage.googleapis.com/csm-artifacts/asm/install_asm_"${ASM_VERSION}" > install_asm_19
    chmod +x install_asm_19
    ```

1.  Create `outdir` folders for the wo clusters. The output of the following scripts are stored in these folders. You also need the version specific `istioctl` CLI utiltiy for later steps.

    ```bash
    mkdir -p ${WORKDIR}/asm-${CLUSTER_1} && mkdir -p ${WORKDIR}/asm-${CLUSTER_1}
    ```

1.  Create ingress gateway customization.

    ```bash
    cat <<EOF > ${WORKDIR}/custom-ingressgateway-prometheus-trace-iap.yaml
    apiVersion: install.istio.io/v1alpha1
    kind: IstioOperator
    spec:
      meshConfig:
        enableTracing: true
      components:
        ingressGateways:
        - name: istio-ingressgateway
          enabled: true
          k8s:
            hpaSpec:
              maxReplicas: 10
              minReplicas: 2
            service:
              type: NodePort
              ports:
                - name: status-port
                  nodePort: 31223
                  port: 15021
                  protocol: TCP
                  targetPort: 15021
                - nodePort: 31224
                  port: 80
                  targetPort: 8080
                  name: http2
                - port: 443
                  targetPort: 8443
                  name: https
                - port: 15012
                  targetPort: 15012
                  name: tcp-istiod
                - port: 15443
                  targetPort: 15443
                  name: tls
      values:
        global:
          proxy:
            tracer: zipkin
          tracer:
            zipkin:
              address: jaeger-collector.monitoring:9411
        telemetry:
          enabled: true
          v2:
            enabled: true
            prometheus:
              enabled: true
    EOF
    ```

1.  Install ASM on both clusters using the `install_asm.sh` script.

    ```bash
    ./install_asm_19 \
    --project_id ${PROJECT_ID} \
    --cluster_name ${CLUSTER_1} \
    --cluster_location ${CLUSTER_1_ZONE} \
    --mode install \
    --option envoy-access-log \
    --option egressgateways \
    --option cloud-tracing \
    --custom_overlay ${WORKDIR}/custom-ingressgateway-prometheus-trace-iap.yaml \
    --output_dir ${WORKDIR}/asm-${CLUSTER_1} \
    --enable_all

    ./install_asm_19 \
    --project_id ${PROJECT_ID} \
    --cluster_name ${CLUSTER_2} \
    --cluster_location ${CLUSTER_2_ZONE} \
    --mode install \
    --option envoy-access-log \
    --option egressgateways \
    --option cloud-tracing \
    --custom_overlay ${WORKDIR}/custom-ingressgateway-prometheus-trace-iap.yaml \
    --output_dir ${WORKDIR}/asm-${CLUSTER_2} \
    --enable_all
    ```

The output is similar to the following:  

    ...
    install_asm: Successfully installed ASM.

1.  Validate ingress gateway customizations.

    ```bash
    kubectl --context=${CLUSTER_1} -n istio-system get svc istio-ingressgateway
    kubectl --context=${CLUSTER_1} -n istio-system get hpa istio-ingressgateway
    kubectl --context=${CLUSTER_2} -n istio-system get svc istio-ingressgateway
    kubectl --context=${CLUSTER_2} -n istio-system get hpa istio-ingressgateway
    ```

The output is similar to the following:  

    # Service does not have an EXTERNAL-IP
    NAME                   TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)                                        AGE
    istio-ingressgateway   ClusterIP   10.92.12.33   <none>        15021/TCP,80/TCP,443/TCP,15012/TCP,15443/TCP   2m9s

    # HPA MAXPODS has changed from 5 to 10
    NAME                   REFERENCE                         TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
    istio-ingressgateway   Deployment/istio-ingressgateway   3%/80%    2         10        2          2m9s

The `EXTERNAL_IP` on the `istio-ingressgateway` Service should be none and the HPA should be updated to the customized value (in this case, the maxReplicas was changed from 5 to 10).  

1.  Get `istioctl` CLI.

    ```bash
    echo -e "export ISTIOCTL_CMD=${WORKDIR}/asm-${CLUSTER_1}/istio-${ISTIO_VERSION}/bin/istioctl" >> $WORKDIR/vars.sh
    source $WORKDIR/vars.sh
    ${ISTIOCTL_CMD} version
    ```

The output is similar to the following:  

    client version: 1.9.1-asm.1
    control plane version: 1.9.1-asm.1
    data plane version: 1.9.1-asm.1 (2 proxies)

### Cross cluster service discovery

1.  Create a secret with the CLUSTER_1 kubeconfig and deploy to CLUSTER_2.

    ```bash
    ${ISTIOCTL_CMD} x create-remote-secret \
    --context=${CLUSTER_1} \
    --name=${CLUSTER_1} > secret-kubeconfig-${CLUSTER_1}.yaml

    kubectl --context=${CLUSTER_2} -n istio-system apply -f secret-kubeconfig-${CLUSTER_1}.yaml
    ```

1.  Create a secret with the CLUSTER_2 kubeconfig and deploy to CLUSTER_1.

    ```bash
    ${ISTIOCTL_CMD} x create-remote-secret \
    --context=${CLUSTER_2} \
    --name=${CLUSTER_2} > secret-kubeconfig-${CLUSTER_2}.yaml

    kubectl --context=${CLUSTER_1} -n istio-system apply -f secret-kubeconfig-${CLUSTER_2}.yaml
    ```

## Installing Prometheus and Grafana from GCP Marketplace

1.  Configure gcloud as a Docker credential helper.

    ```bash
    gcloud auth configure-docker
    ```

1.  Clone this repo and the associated tools repo.

    ```bash
    git clone --recursive https://github.com/GoogleCloudPlatform/click-to-deploy.git
    ```

An Application resource is a collection of individual Kubernetes components, such as Services, Deployments, and so on, that you can manage as a group.

1.  To set up your cluster to understand Application resources, run the following command.

    ```bash
    kubectl --context=${CLUSTER_1} apply -f "https://raw.githubusercontent.com/GoogleCloudPlatform/marketplace-k8s-app-tools/master/crd/app-crd.yaml"
    kubectl --context=${CLUSTER_2} apply -f "https://raw.githubusercontent.com/GoogleCloudPlatform/marketplace-k8s-app-tools/master/crd/app-crd.yaml"

    cd $WORKDIR/click-to-deploy/k8s/prometheus
    echo -e "export APP_INSTANCE_NAME=prometheus-1" >> $WORKDIR/vars.sh
    echo -e "export NAMESPACE=monitoring" >> $WORKDIR/vars.sh
    source $WORKDIR/vars.sh
    ```

1.  Configure the container images.

    ```bash
    echo -e "TAG=2.11" >> $WORKDIR/vars.sh && source $WORKDIR/vars.sh
    echo -e "export IMAGE_PROMETHEUS=marketplace.gcr.io/google/prometheus:${TAG}" >> $WORKDIR/vars.sh
    echo -e "export IMAGE_ALERTMANAGER=marketplace.gcr.io/google/prometheus/alertmanager:${TAG}" >> $WORKDIR/vars.sh
    echo -e "export IMAGE_KUBE_STATE_METRICS=marketplace.gcr.io/google/prometheus/kubestatemetrics:${TAG}" >> $WORKDIR/vars.sh
    echo -e "export IMAGE_NODE_EXPORTER=marketplace.gcr.io/google/prometheus/nodeexporter:${TAG}" >> $WORKDIR/vars.sh
    echo -e "export IMAGE_GRAFANA=marketplace.gcr.io/google/prometheus/grafana:${TAG}" >> $WORKDIR/vars.sh
    echo -e "export IMAGE_PROMETHEUS_INIT=marketplace.gcr.io/google/prometheus/debian9:${TAG}" >> $WORKDIR/vars.sh
    source $WORKDIR/vars.sh
    ```

1.  The images above are referenced by tag. We recommend that you pin each image to an immutable content digest. This ensures that the installed application always uses the same images, until you are ready to upgrade. To get the digest for the image, use the following script

    ```bash
    for i in "IMAGE_PROMETHEUS" \
      "IMAGE_ALERTMANAGER" \
      "IMAGE_KUBE_STATE_METRICS" \
      "IMAGE_NODE_EXPORTER" \
      "IMAGE_GRAFANA" \
      "IMAGE_PROMETHEUS_INIT"; do
        repo=$(echo ${!i} | cut -d: -f1);
        digest=$(docker pull ${!i} | sed -n -e 's/Digest: //p');
        echo -e "export $i=$repo@$digest" >> $WORKDIR/vars.sh;
        env | grep $i;
    done
    source $WORKDIR/vars.sh
    ```

1.  Generate a random password for Grafana.

    ```bash
    # Install pwgen and base64
    sudo apt-get update && sudo apt-get install -y pwgen

    # Set the Grafana password
    echo -e "export GRAFANA_GENERATED_PASSWORD=$(pwgen 12 1 | tr -d '\n' | base64)" >> $WORKDIR/vars.sh
    ```

1.  Define size of Prometheus StatefulSet and storage class.

    ```bash
    echo -e "export PROMETHEUS_REPLICAS=2" >> $WORKDIR/vars.sh
    echo -e "export STORAGE_CLASS=standard" >> $WORKDIR/vars.sh
    source $WORKDIR/vars.sh
    ```

1.  Create the namespace.

    ```bash
    kubectl --context=${CLUSTER_1} create namespace "$NAMESPACE"
    kubectl --context=${CLUSTER_2} create namespace "$NAMESPACE"
    ```

1.  Define environment variables for the application.

    ```bash
    echo -e "export PROMETHEUS_SERVICE_ACCOUNT=${APP_INSTANCE_NAME}-prometheus" >> $WORKDIR/vars.sh
    echo -e "export KUBE_STATE_METRICS_SERVICE_ACCOUNT=${APP_INSTANCE_NAME}-kube-state-metrics" >> $WORKDIR/vars.sh
    echo -e "export ALERTMANAGER_SERVICE_ACCOUNT=${APP_INSTANCE_NAME}-alertmanager" >> $WORKDIR/vars.sh
    echo -e "export GRAFANA_SERVICE_ACCOUNT=${APP_INSTANCE_NAME}-grafana" >> $WORKDIR/vars.sh
    echo -e "export NODE_EXPORTER_SERVICE_ACCOUNT=${APP_INSTANCE_NAME}-node-exporter" >> $WORKDIR/vars.sh
    source $WORKDIR/vars.sh
    ```

1.  Expand the manifests to create Service Accounts.

    ```bash
    cat resources/service-accounts.yaml \
    | envsubst '$NAMESPACE $PROMETHEUS_SERVICE_ACCOUNT $KUBE_STATE_METRICS_SERVICE_ACCOUNT $ALERTMANAGER_SERVICE_ACCOUNT $GRAFANA_SERVICE_ACCOUNT $NODE_EXPORTER_SERVICE_ACCOUNT' \
    > "${APP_INSTANCE_NAME}_sa_manifest.yaml"

    kubectl --context=${CLUSTER_1} apply -f "${APP_INSTANCE_NAME}_sa_manifest.yaml" \
    --namespace "${NAMESPACE}"
    kubectl --context=${CLUSTER_2} apply -f "${APP_INSTANCE_NAME}_sa_manifest.yaml" \
    --namespace "${NAMESPACE}"
    ```

1.  Use envsubst to expand the template. We recommend that you save the expanded manifest file for future updates to the application. Apply the manifests to both application clusters.

    ```bash
    awk 'FNR==1 {print "---"}{print}' manifest/* \
    | envsubst '$APP_INSTANCE_NAME $NAMESPACE $STORAGE_CLASS $IMAGE_PROMETHEUS $IMAGE_ALERTMANAGER $IMAGE_KUBE_STATE_METRICS $IMAGE_NODE_EXPORTER $IMAGE_GRAFANA $IMAGE_PROMETHEUS_INIT $NAMESPACE $GRAFANA_GENERATED_PASSWORD $PROMETHEUS_REPLICAS $PROMETHEUS_REPLICAS $PROMETHEUS_SERVICE_ACCOUNT $KUBE_STATE_METRICS_SERVICE_ACCOUNT $ALERTMANAGER_SERVICE_ACCOUNT $GRAFANA_SERVICE_ACCOUNT $NODE_EXPORTER_SERVICE_ACCOUNT' \
    > "${APP_INSTANCE_NAME}_manifest.yaml"

    kubectl --context=${CLUSTER_1} apply -f "${APP_INSTANCE_NAME}_manifest.yaml" --namespace "${NAMESPACE}"
    kubectl --context=${CLUSTER_2} apply -f "${APP_INSTANCE_NAME}_manifest.yaml" --namespace "${NAMESPACE}"
    ```

1.  To get the GCP Console URL for your app, run the following command

    ```bash
    echo "https://console.cloud.google.com/kubernetes/application/${ZONE}/${CLUSTER}/${NAMESPACE}/${APP_INSTANCE_NAME}"
    cd $WORKDIR
    ```

1.  Ensure all resources are available.

    ```bash
    kubectl --context=${CLUSTER_1} -n ${NAMESPACE} wait --for=condition=Available deployment prometheus-1-kube-state-metrics
    kubectl --context=${CLUSTER_1} -n ${NAMESPACE} rollout status --watch --timeout=600s statefulset/prometheus-1-alertmanager
    kubectl --context=${CLUSTER_1} -n ${NAMESPACE} rollout status --watch --timeout=600s statefulset/prometheus-1-grafana
    kubectl --context=${CLUSTER_1} -n ${NAMESPACE} rollout status --watch --timeout=600s statefulset/prometheus-1-prometheus

    kubectl --context=${CLUSTER_2} -n ${NAMESPACE} wait --for=condition=Available deployment prometheus-1-kube-state-metrics
    kubectl --context=${CLUSTER_2} -n ${NAMESPACE} rollout status --watch --timeout=600s statefulset/prometheus-1-alertmanager
    kubectl --context=${CLUSTER_2} -n ${NAMESPACE} rollout status --watch --timeout=600s statefulset/prometheus-1-grafana
    kubectl --context=${CLUSTER_2} -n ${NAMESPACE} rollout status --watch --timeout=600s statefulset/prometheus-1-prometheus
    ```

### Accessing Prometheus and Grafana

1.  Get Grafana login credentials from CLUSTER_1

    ```bash
    CLUSTER_1_GRAFANA_USERNAME="$(kubectl --context=${CLUSTER_1} get secret $APP_INSTANCE_NAME-grafana \
    --namespace $NAMESPACE \
    --output=jsonpath='{.data.admin-user}' | base64 --decode)"
    CLUSTER_1_GRAFANA_PASSWORD="$(kubectl --context=${CLUSTER_1} get secret $APP_INSTANCE_NAME-grafana \
    --namespace $NAMESPACE \
    --output=jsonpath='{.data.admin-password}' | base64 --decode)"
    echo "Grafana credentials:"
    echo "- user: ${CLUSTER_1_GRAFANA_USERNAME}"
    echo "- pass: ${CLUSTER_1_GRAFANA_PASSWORD}"

    echo -e "export CLUSTER_1_GRAFANA_USERNAME=${CLUSTER_1_GRAFANA_USERNAME}" >> $WORKDIR/vars.sh
    echo -e "export CLUSTER_1_GRAFANA_PASSWORD=${CLUSTER_1_GRAFANA_PASSWORD}" >> $WORKDIR/vars.sh
    ```

1.  Get Grafana login credentials from CLUSTER_2

    ```bash
    CLUSTER_2_GRAFANA_USERNAME="$(kubectl --context=${CLUSTER_2} get secret $APP_INSTANCE_NAME-grafana \
    --namespace $NAMESPACE \
    --output=jsonpath='{.data.admin-user}' | base64 --decode)"
    CLUSTER_2_GRAFANA_PASSWORD="$(kubectl --context=${CLUSTER_2} get secret $APP_INSTANCE_NAME-grafana \
    --namespace $NAMESPACE \
    --output=jsonpath='{.data.admin-password}' | base64 --decode)"
    echo "Grafana credentials:"
    echo "- user: ${CLUSTER_2_GRAFANA_USERNAME}"
    echo "- pass: ${CLUSTER_2_GRAFANA_PASSWORD}"

    echo -e "export CLUSTER_2_GRAFANA_USERNAME=${CLUSTER_2_GRAFANA_USERNAME}" >> $WORKDIR/vars.sh
    echo -e "export CLUSTER_2_GRAFANA_PASSWORD=${CLUSTER_2_GRAFANA_PASSWORD}" >> $WORKDIR/vars.sh
    ```

1.  Expose and access Grafana using port-forward. Later you use [Identity Aware Proxy](https://cloud.google.com/iap) to expose all telemetry tools behind OAuth login workflow.

    ```bash
    kubectl --context=${CLUSTER_1} port-forward --namespace ${NAMESPACE} prometheus-1-grafana-0 3000 --address 0.0.0.0 &
    ```

> It may take a few moments to login to Grafana. Until then you may get the login prompt with a `login failed` message. Wait a few moments and retry until you can login before proceeding on with the following steps.  

1.  With port-forwarding, add Prometheus as datasource to Grafana and add all of the Istio dashboards.

    ```bash
    # Address of Grafana
    GRAFANA_HOST="http://localhost:3000"
    # Login credentials, if authentication is used
    GRAFANA_CRED="${CLUSTER_1_GRAFANA_USERNAME}:${CLUSTER_1_GRAFANA_PASSWORD}"
    # The name of the Prometheus data source to use
    GRAFANA_DATASOURCE="prometheus"
    # The version of Istio to deploy
    VERSION=1.9.1
    # Import all Istio dashboards
    for DASHBOARD in 7639 11829 7636 7630 7645; do
      REVISION="$(curl -s "https://grafana.com/api/dashboards/${DASHBOARD}/revisions" -s | jq ".items[] | select(.description | contains(\"${VERSION}\")) | .revision")"
      curl -s "https://grafana.com/api/dashboards/${DASHBOARD}/revisions/${REVISION}/download" > $WORKDIR/dashboard.json
      echo "Importing $(cat $WORKDIR/dashboard.json | jq -r '.title') (revision ${REVISION}, id ${DASHBOARD})..."
      export DASH_ID=$(curl -s -k -u "$GRAFANA_CRED" -XPOST \
      -H "Accept: application/json" \
      -H "Content-Type: application/json" \
      -d "{\"dashboard\":$(cat $WORKDIR/dashboard.json),\"overwrite\":true, \
      \"inputs\":[{\"name\":\"DS_PROMETHEUS\",\"type\":\"datasource\", \
      \"pluginId\":\"prometheus\",\"value\":\"$GRAFANA_DATASOURCE\"}]}" \
      $GRAFANA_HOST/api/dashboards/import | jq -r '.dashboardId')
      # Star all the imported dashboards
      curl -s -k -u "$GRAFANA_CRED" -XPOST \
      -H "Accept: application/json" \
      -H "Content-Type: application/json" \
      $GRAFANA_HOST/api/user/stars/dashboard/${DASH_ID}
      echo -e "\nDone\n"
    done
    ```

The output is similar to the following:  

    Importing Istio Service Dashboard (revision 56, id 7636)...

1.  You can access the Grafana admin UI by navigating to the following link and logging in with the credentials you received above.

    ```bash
    echo -e "http://localhost:3000"
    ```

You can now see your Istio dashboard. Later you add some applications to see metrics.  

## Installing Jaeger

1.  Deploy Jaeger via helm chart.

    ```bash
    kubectl ctx ${CLUSTER_1}
    helm repo add jaegertracing https://jaegertracing.github.io/helm-charts

    helm install \
    --namespace ${NAMESPACE} \
    --set collector.service.zipkin.port=9411 \
    jaeger \
    jaegertracing/jaeger

    kubectl ctx ${CLUSTER_2}
    helm repo add jaegertracing https://jaegertracing.github.io/helm-charts

    helm install \
    --namespace ${NAMESPACE} \
    --set collector.service.zipkin.port=9411 \
    jaeger \
    jaegertracing/jaeger
    ```

Jaeger components take a few moments to come up. Ensure all Yaeger Pods (including the Cassandra StatefulSet Pods) are up and Running before proceeding.  

1.  Verify Jaeger is fully functional and Jaeger query (UI) Deployment is Available.

    ```bash
    kubectl --context=${CLUSTER_1} -n ${NAMESPACE} wait --for=condition=available deployment jaeger-query --timeout=10m
    kubectl --context=${CLUSTER_2} -n ${NAMESPACE} wait --for=condition=available deployment jaeger-query --timeout=10m
    ```

The output is similar to the following:  

    deployment.apps/jaeger-query condition met

1.  Expose Jaeger query using port-forward.

    ```bash
    export JAEGER_QUERY_POD_NAME=$(kubectl --context=${CLUSTER_1} get pods --namespace ${NAMESPACE} -l "app.kubernetes.io/instance=jaeger,app.kubernetes.io/component=query" -o jsonpath="{.items[0].metadata.name}")
    kubectl --context=${CLUSTER_1} port-forward --namespace ${NAMESPACE} ${JAEGER_QUERY_POD_NAME} 8000:16686 --address 0.0.0.0
    ```

You should be able to access the Jaeger UI but you will not see any services until you deploy applications.  

## Installing Kiali

1.  Use Helm to deploy Kiali in both clusters.

    ```bash
    kubectl ctx ${CLUSTER_1}
    helm install \
    --namespace ${NAMESPACE} \
    --set auth.strategy="anonymous" \
    --set istio_namespace="istio-system" \
    --set external_services.prometheus.url="http://prometheus-1-prometheus:9090" \
    --set external_services.grafana.url="http://prometheus-1-grafana:3000" \
    --set external_services.grafana.in_cluster_url="http://prometheus-1-grafana:3000" \
    --set external_services.tracing.url="http://jaeger-query" \
    --set external_services.tracing.in_cluster_url="http://jaeger-query" \
    --set external_services.custom_dashboards.prometheus.url="http://prometheus-1-prometheus:9090" \
    --set external_services.custom_dashboards.namespace_label="kubernetes_namespace" \
    --repo https://kiali.org/helm-charts \
    kiali-server \
    kiali-server

    kubectl ctx ${CLUSTER_2}
    helm install \
    --namespace ${NAMESPACE} \
    --set auth.strategy="anonymous" \
    --set istio_namespace="istio-system" \
    --set external_services.prometheus.url="http://prometheus-1-prometheus:9090" \
    --set external_services.grafana.url="http://prometheus-1-grafana:3000" \
    --set external_services.grafana.in_cluster_url="http://prometheus-1-grafana:3000" \
    --set external_services.tracing.url="http://jaeger-query" \
    --set external_services.tracing.in_cluster_url="http://jaeger-query" \
    --set external_services.custom_dashboards.prometheus.url="http://prometheus-1-prometheus:9090" \
    --set external_services.custom_dashboards.namespace_label="kubernetes_namespace" \
    --repo https://kiali.org/helm-charts \
    kiali-server \
    kiali-server
    ```

1.  Ensure Kiali resources are ready.

    ```bash
    kubectl --context=${CLUSTER_1} --namespace ${NAMESPACE} wait --for=condition=available deployment kiali --timeout=10m
    kubectl --context=${CLUSTER_2} --namespace ${NAMESPACE} wait --for=condition=available deployment kiali --timeout=10m
    ```

The output is similar to the following:  

    deployment.apps/kiali condition met

1.  Expose and access Kiali using port-forward.

    ```bash
    kubectl --context=${CLUSTER_1} port-forward --namespace ${NAMESPACE} svc/kiali 8080:20001 --address 0.0.0.0
    ```

## Bank of Anthos

In this section, you deploy Bank of Anthos to CLUSTER_1 and CLUSTER_2.  

1.  Clone the Bank of Anthos repo.

    ```bash
    git clone https://github.com/GoogleCloudPlatform/bank-of-anthos.git bank-of-anthos
    ```

1.  Create and label `bank-of-anthos` namespace in both cluster. The ASM label ensures that the Envoy proxies get automatically injected into each Pod in that namespace.

    ```bash
    kubectl create --context=${CLUSTER_1} namespace bank-of-anthos
    kubectl label --context=${CLUSTER_1} namespace bank-of-anthos istio.io/rev=${ASM_LABEL}
    kubectl create --context=${CLUSTER_2} namespace bank-of-anthos
    kubectl label --context=${CLUSTER_2} namespace bank-of-anthos istio.io/rev=${ASM_LABEL}
    ```

1.  Create a Google Service Account to set it up with Workload Identity. With Workload Identity, Kubernetes Service Accounts can authenticate and access Google APIs. Bank of Anthos requires access to Cloud Ops for metrics and traces.

    ```bash
    gcloud iam service-accounts create ${GSA_NAME}
    kubectl --context=${CLUSTER_1} create serviceaccount --namespace bank-of-anthos ${KSA_NAME}
    kubectl --context=${CLUSTER_2} create serviceaccount --namespace bank-of-anthos ${KSA_NAME}
    ```

1.  Create IAM roles.

    ```bash
    gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member "serviceAccount:${GSA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com" \
    --role roles/cloudtrace.agent

    gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member "serviceAccount:${GSA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com" \
    --role roles/monitoring.metricWriter

    gcloud iam service-accounts add-iam-policy-binding \
    --role roles/iam.workloadIdentityUser \
    --member "serviceAccount:${PROJECT_ID}.svc.id.goog[bank-of-anthos/${KSA_NAME}]" \
    ${GSA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com

    kubectl --context=$CLUSTER_1 annotate serviceaccount \
    --namespace bank-of-anthos \
    ${KSA_NAME} \
    iam.gke.io/gcp-service-account=${GSA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com

    kubectl --context=$CLUSTER_2 annotate serviceaccount \
    --namespace bank-of-anthos \
    ${KSA_NAME} \
    iam.gke.io/gcp-service-account=${GSA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com

    mkdir -p ${WORKDIR}/wi-kubernetes-manifests
    FILES="${WORKDIR}/bank-of-anthos/kubernetes-manifests/*"
    for f in $FILES; do
      echo "Processing $f..."
      sed -e "s/serviceAccountName: default/serviceAccountName: ${KSA_NAME}/g" -e "s/0.4.3/0.4.2/g" $f > ${WORKDIR}/wi-kubernetes-manifests/`basename $f`
    done
    ```

1.  Deploy Bank of Anthos application to both cluisters in the `bank-of-anthos` namespace. Deploy the two PostGres DBs only in one of two clusters by deleting the StatefulSets from the other cluster.

    ```bash
    kubectl --context=$CLUSTER_1 -n bank-of-anthos apply -f bank-of-anthos/extras/jwt/jwt-secret.yaml
    kubectl --context=$CLUSTER_2 -n bank-of-anthos apply -f bank-of-anthos/extras/jwt/jwt-secret.yaml
    kubectl --context=$CLUSTER_1 -n bank-of-anthos apply -f ./wi-kubernetes-manifests
    kubectl --context=$CLUSTER_2 -n bank-of-anthos apply -f ./wi-kubernetes-manifests
    kubectl --context=$CLUSTER_2 -n bank-of-anthos delete statefulset accounts-db
    kubectl --context=$CLUSTER_2 -n bank-of-anthos delete statefulset ledger-db
    ```

All Services are deployed as Distributed Services. This means that there are Deployments of all Services on both clusters. The `accounts-db` and `ledger-db` is only deployed to CLUSTER1.  

1.  Verify that all Pods are running in both clusters.

    ```bash
    kubectl --context=${CLUSTER_1} -n bank-of-anthos wait --for=condition=ready --timeout=5m pod accounts-db-0
    kubectl --context=${CLUSTER_1} -n bank-of-anthos wait --for=condition=ready --timeout=5m pod ledger-db-0
    kubectl --context=${CLUSTER_1} -n bank-of-anthos wait --for=condition=available --timeout=5m deployment contacts
    kubectl --context=${CLUSTER_1} -n bank-of-anthos wait --for=condition=available --timeout=5m deployment frontend
    kubectl --context=${CLUSTER_1} -n bank-of-anthos wait --for=condition=available --timeout=5m deployment ledgerwriter
    kubectl --context=${CLUSTER_1} -n bank-of-anthos wait --for=condition=available --timeout=5m deployment loadgenerator
    kubectl --context=${CLUSTER_1} -n bank-of-anthos wait --for=condition=available --timeout=5m deployment balancereader
    kubectl --context=${CLUSTER_1} -n bank-of-anthos wait --for=condition=available --timeout=5m deployment transactionhistory
    kubectl --context=${CLUSTER_1} -n bank-of-anthos wait --for=condition=available --timeout=5m deployment userservice

    kubectl --context=${CLUSTER_2} -n bank-of-anthos wait --for=condition=available --timeout=5m deployment contacts
    kubectl --context=${CLUSTER_2} -n bank-of-anthos wait --for=condition=available --timeout=5m deployment frontend
    kubectl --context=${CLUSTER_2} -n bank-of-anthos wait --for=condition=available --timeout=5m deployment ledgerwriter
    kubectl --context=${CLUSTER_2} -n bank-of-anthos wait --for=condition=available --timeout=5m deployment loadgenerator
    kubectl --context=${CLUSTER_2} -n bank-of-anthos wait --for=condition=available --timeout=5m deployment balancereader
    kubectl --context=${CLUSTER_2} -n bank-of-anthos wait --for=condition=available --timeout=5m deployment transactionhistory
    kubectl --context=${CLUSTER_2} -n bank-of-anthos wait --for=condition=available --timeout=5m deployment userservice
    ```

The output is similar to the following:  

    deployment.apps/frontend condition met

1.  Get Pods from both clusters.

    ```bash
    kubectl --context=${CLUSTER_1} -n bank-of-anthos get pod
    kubectl --context=${CLUSTER_2} -n bank-of-anthos get pod
    ```

The output is similar to the following:  

    # CLUSTER1
    NAME                                  READY   STATUS    RESTARTS   AGE
    accounts-db-0                         2/2     Running   0          2m40s
    balancereader-c5d664b4c-tf8fg         2/2     Running   0          2m40s
    contacts-7fd8c5fb6-vprp9              2/2     Running   1          2m39s
    frontend-7b7fb9b665-g9wzd             2/2     Running   1          2m39s
    ledger-db-0                           2/2     Running   0          2m38s
    ledgerwriter-7b5b6db66f-2hsqt         2/2     Running   0          2m38s
    loadgenerator-7fb54d57f8-85bdc        2/2     Running   0          2m37s
    transactionhistory-7fdb998c5f-ns29r   2/2     Running   0          2m37s
    userservice-76996974f5-tv56x          2/2     Running   0          2m37s

    # CLUSTER2
    NAME                                  READY   STATUS    RESTARTS   AGE
    balancereader-c5d664b4c-vwjrw         2/2     Running   1          2m37s
    contacts-7fd8c5fb6-5c2l9              2/2     Running   1          2m36s
    frontend-7b7fb9b665-gjllh             2/2     Running   1          2m36s
    ledgerwriter-7b5b6db66f-lp2vn         2/2     Running   0          2m35s
    loadgenerator-7fb54d57f8-wpsct        2/2     Running   0          2m34s
    transactionhistory-7fdb998c5f-8ndqx   2/2     Running   0          2m34s
    userservice-76996974f5-n6xwp          2/2     Running   1          2m34s

1.  Get the frontend Pod from CLUSTER1.

    ```bash
    export BANK_CLUSTER_1_FRONTEND_POD=$(kubectl get pod -n bank-of-anthos -l app=frontend --context=${CLUSTER_1} -o jsonpath='{.items[0].metadata.name}')
    ```

1.  Inspect the proxy-config endpoints on the frontend Pod.

    ```bash
    ${ISTIOCTL_CMD} --context $CLUSTER_1 -n bank-of-anthos pc ep $BANK_CLUSTER_1_FRONTEND_POD | grep bank-of-anthos
    ```

The output is similar to the following:  

    10.0.0.10:8080                   HEALTHY     OK                outbound|80||frontend.bank-of-anthos.svc.cluster.local
    10.0.0.11:8080                   HEALTHY     OK                outbound|8080||userservice.bank-of-anthos.svc.cluster.local
    10.0.1.6:5432                    HEALTHY     OK                outbound|5432||accounts-db.bank-of-anthos.svc.cluster.local
    10.0.1.7:8080                    HEALTHY     OK                outbound|8080||contacts.bank-of-anthos.svc.cluster.local
    10.0.2.10:8080                   HEALTHY     OK                outbound|8080||ledgerwriter.bank-of-anthos.svc.cluster.local
    10.0.2.12:8080                   HEALTHY     OK                outbound|8080||transactionhistory.bank-of-anthos.svc.cluster.local
    10.0.2.8:8080                    HEALTHY     OK                outbound|8080||balancereader.bank-of-anthos.svc.cluster.local
    10.0.2.9:5432                    HEALTHY     OK                outbound|5432||ledger-db.bank-of-anthos.svc.cluster.local
    10.0.3.7:8080                    HEALTHY     OK                outbound|8080||balancereader.bank-of-anthos.svc.cluster.local
    10.0.3.8:8080                    HEALTHY     OK                outbound|8080||contacts.bank-of-anthos.svc.cluster.local
    10.0.3.9:8080                    HEALTHY     OK                outbound|8080||userservice.bank-of-anthos.svc.cluster.local
    10.0.4.11:8080                   HEALTHY     OK                outbound|8080||ledgerwriter.bank-of-anthos.svc.cluster.local
    10.0.4.12:8080                   HEALTHY     OK                outbound|8080||transactionhistory.bank-of-anthos.svc.cluster.local
    10.0.5.8:8080                    HEALTHY     OK                outbound|80||frontend.bank-of-anthos.svc.cluster.local

With the exception of `accounts-db` and `ledger-db`, all other Services have two Endpoints. One Pod in each cluster.   

## Online Boutique

In this section, you deploy Online Boutique to CLUSTER1 and CLUSTER2.  

1.  Clone the Online Boutique repo.

    ```bash
    git clone https://github.com/GoogleCloudPlatform/microservices-demo online-boutique
    ```

1.  Create and label `online-boutique` namespace in both cluster. The ASM label ensures that the Envoy proxies get automatically injected into each Pod in that namespace.

    ```bash
    kubectl create --context=${CLUSTER_1} namespace online-boutique
    kubectl label --context=${CLUSTER_1} namespace online-boutique istio.io/rev=${ASM_LABEL}
    kubectl create --context=${CLUSTER_2} namespace online-boutique
    kubectl label --context=${CLUSTER_2} namespace online-boutique istio.io/rev=${ASM_LABEL}
    ```

1.  Deploy Online Boutique to both cluisters in the `online-boutique` namespace. Deploy redis-cart Deployment only in one of two clusters. Redis is used by cartservice to store items in your shopping cart. It is the only stateful service in Online Boutique.

    ```bash
    kubectl --context=$CLUSTER_1 -n online-boutique apply -f online-boutique/release
    kubectl --context=$CLUSTER_2 -n online-boutique apply -f online-boutique/release
    kubectl --context=$CLUSTER_2 -n online-boutique delete deployment redis-cart
    kubectl --context=${CLUSTER_1} -n online-boutique delete gateway frontend-gateway
    kubectl --context=${CLUSTER_2} -n online-boutique delete gateway frontend-gateway
    kubectl --context=${CLUSTER_1} -n online-boutique delete virtualservice frontend-ingress
    kubectl --context=${CLUSTER_2} -n online-boutique delete virtualservice frontend-ingress
    ```

All Services are deployed as Distributed Services. This means that there are Deployments of all Services on both clusters. The `redis-cart` is only deployed to CLUSTER_1.  

1.  Verify that all Pods are running in both clusters.

    ```bash
    kubectl --context=${CLUSTER_1} -n online-boutique wait --for=condition=available --timeout=5m deployment adservice
    kubectl --context=${CLUSTER_1} -n online-boutique wait --for=condition=available --timeout=5m deployment cartservice
    kubectl --context=${CLUSTER_1} -n online-boutique wait --for=condition=available --timeout=5m deployment checkoutservice
    kubectl --context=${CLUSTER_1} -n online-boutique wait --for=condition=available --timeout=5m deployment currencyservice
    kubectl --context=${CLUSTER_1} -n online-boutique wait --for=condition=available --timeout=5m deployment emailservice
    kubectl --context=${CLUSTER_1} -n online-boutique wait --for=condition=available --timeout=5m deployment frontend
    kubectl --context=${CLUSTER_1} -n online-boutique wait --for=condition=available --timeout=5m deployment loadgenerator
    kubectl --context=${CLUSTER_1} -n online-boutique wait --for=condition=available --timeout=5m deployment paymentservice
    kubectl --context=${CLUSTER_1} -n online-boutique wait --for=condition=available --timeout=5m deployment productcatalogservice
    kubectl --context=${CLUSTER_1} -n online-boutique wait --for=condition=available --timeout=5m deployment recommendationservice
    kubectl --context=${CLUSTER_1} -n online-boutique wait --for=condition=available --timeout=5m deployment redis-cart
    kubectl --context=${CLUSTER_1} -n online-boutique wait --for=condition=available --timeout=5m deployment shippingservice

    kubectl --context=${CLUSTER_2} -n online-boutique wait --for=condition=available --timeout=5m deployment adservice
    kubectl --context=${CLUSTER_2} -n online-boutique wait --for=condition=available --timeout=5m deployment cartservice
    kubectl --context=${CLUSTER_2} -n online-boutique wait --for=condition=available --timeout=5m deployment checkoutservice
    kubectl --context=${CLUSTER_2} -n online-boutique wait --for=condition=available --timeout=5m deployment currencyservice
    kubectl --context=${CLUSTER_2} -n online-boutique wait --for=condition=available --timeout=5m deployment emailservice
    kubectl --context=${CLUSTER_2} -n online-boutique wait --for=condition=available --timeout=5m deployment frontend
    kubectl --context=${CLUSTER_2} -n online-boutique wait --for=condition=available --timeout=5m deployment loadgenerator
    kubectl --context=${CLUSTER_2} -n online-boutique wait --for=condition=available --timeout=5m deployment paymentservice
    kubectl --context=${CLUSTER_2} -n online-boutique wait --for=condition=available --timeout=5m deployment productcatalogservice
    kubectl --context=${CLUSTER_2} -n online-boutique wait --for=condition=available --timeout=5m deployment recommendationservice
    kubectl --context=${CLUSTER_2} -n online-boutique wait --for=condition=available --timeout=5m deployment shippingservice
    ```

The output is similar to the following:  

    deployment.apps/frontend condition met

1.  Get Pods from CLUSTER1.

    ```bash
    kubectl --context=${CLUSTER_1} -n online-boutique get pod
    ```

1.  Get Pods from CLUSTER2.

    ```bash
    kubectl --context=${CLUSTER_2} -n online-boutique get pod
    ```

The output is similar to the following:  

    # CLUSTER_1
    NAME                                     READY   STATUS    RESTARTS   AGE
    adservice-76bdd69666-zcsjk               2/2     Running   0          3m12s
    cartservice-66d497c6b7-5vgr2             2/2     Running   1          3m14s
    checkoutservice-666c784bd6-tsx9k         2/2     Running   0          3m15s
    currencyservice-5d5d496984-ghfs5         2/2     Running   0          3m13s
    emailservice-667457d9d6-7zn6m            2/2     Running   0          3m15s
    frontend-6b8d69b9fb-mgx9w                2/2     Running   0          3m15s
    loadgenerator-665b5cd444-l2nrf           2/2     Running   2          3m13s
    paymentservice-68596d6dd6-b47lq          2/2     Running   0          3m14s
    productcatalogservice-557d474574-bs6p5   2/2     Running   0          3m14s
    recommendationservice-69c56b74d4-9zk7l   2/2     Running   0          3m15s
    redis-cart-5f59546cdd-bgpvh              2/2     Running   0          3m12s
    shippingservice-6ccc89f8fd-v6q7l         2/2     Running   0          3m13s

    # CLUSTER_2
    NAME                                     READY   STATUS    RESTARTS   AGE
    adservice-76bdd69666-s7t79               2/2     Running   0          3m6s
    cartservice-66d497c6b7-5lpwp             2/2     Running   1          3m7s
    checkoutservice-666c784bd6-9vwb4         2/2     Running   0          3m9s
    currencyservice-5d5d496984-62fbr         2/2     Running   0          3m7s
    emailservice-667457d9d6-t95b5            2/2     Running   0          3m9s
    frontend-6b8d69b9fb-xppk2                2/2     Running   0          3m9s
    loadgenerator-665b5cd444-98hpm           2/2     Running   2          3m7s
    paymentservice-68596d6dd6-f2jvp          2/2     Running   0          3m8s
    productcatalogservice-557d474574-wqndp   2/2     Running   0          3m8s
    recommendationservice-69c56b74d4-vwt79   2/2     Running   0          3m9s
    shippingservice-6ccc89f8fd-rlhcc         2/2     Running   0          3m7

1.  Get the frontend Pod from CLUSTER1.

    ```bash
    export SHOP_CLUSTER1_FRONTEND_POD=$(kubectl get pod -n online-boutique -l app=frontend --context=${CLUSTER_1} -o jsonpath='{.items[0].metadata.name}')
    ```

1.  Inspect the proxy-config endpoints on the frontend Pod.

    ```bash
    ${ISTIOCTL_CMD} --context $CLUSTER_1 -n online-boutique pc ep $SHOP_CLUSTER1_FRONTEND_POD | grep online-boutique
    ```

The output is similar to the following:  

    10.8.0.7:50051                   HEALTHY     OK                outbound|50051||paymentservice.online-boutique.svc.cluster.local
    10.8.0.8:7070                    HEALTHY     OK                outbound|7070||cartservice.online-boutique.svc.cluster.local
    10.8.0.9:6379                    HEALTHY     OK                outbound|6379||redis-cart.online-boutique.svc.cluster.local
    10.8.1.10:8080                   HEALTHY     OK                outbound|8080||recommendationservice.online-boutique.svc.cluster.local
    10.8.1.11:5050                   HEALTHY     OK                outbound|5050||checkoutservice.online-boutique.svc.cluster.local
    10.8.1.13:7000                   HEALTHY     OK                outbound|7000||currencyservice.online-boutique.svc.cluster.local
    10.8.2.10:50051                  HEALTHY     OK                outbound|50051||shippingservice.online-boutique.svc.cluster.local
    10.8.2.11:9555                   HEALTHY     OK                outbound|9555||adservice.online-boutique.svc.cluster.local
    10.8.2.7:8080                    HEALTHY     OK                outbound|80||frontend-external.online-boutique.svc.cluster.local
    10.8.2.7:8080                    HEALTHY     OK                outbound|80||frontend.online-boutique.svc.cluster.local
    10.8.2.8:8080                    HEALTHY     OK                outbound|5000||emailservice.online-boutique.svc.cluster.local
    10.8.2.9:3550                    HEALTHY     OK                outbound|3550||productcatalogservice.online-boutique.svc.cluster.local
    10.8.3.5:8080                    HEALTHY     OK                outbound|8080||recommendationservice.online-boutique.svc.cluster.local
    10.8.3.6:50051                   HEALTHY     OK                outbound|50051||paymentservice.online-boutique.svc.cluster.local
    10.8.3.7:5050                    HEALTHY     OK                outbound|5050||checkoutservice.online-boutique.svc.cluster.local
    10.8.3.8:7070                    HEALTHY     OK                outbound|7070||cartservice.online-boutique.svc.cluster.local
    10.8.3.9:7000                    HEALTHY     OK                outbound|7000||currencyservice.online-boutique.svc.cluster.local
    10.8.4.9:50051                   HEALTHY     OK                outbound|50051||shippingservice.online-boutique.svc.cluster.local
    10.8.5.10:8080                   HEALTHY     OK                outbound|80||frontend-external.online-boutique.svc.cluster.local
    10.8.5.10:8080                   HEALTHY     OK                outbound|80||frontend.online-boutique.svc.cluster.local
    10.8.5.11:8080                   HEALTHY     OK                outbound|5000||emailservice.online-boutique.svc.cluster.local
    10.8.5.13:9555                   HEALTHY     OK                outbound|9555||adservice.online-boutique.svc.cluster.local
    10.8.5.9:3550                    HEALTHY     OK                outbound|3550||productcatalogservice.online-boutique.svc.cluster.local

With the exception of `redis-cart`, all other Services have two Endpoints. One Pod in each cluster.   

## Configure secure MulticlusterIngress for applications with DNS and Google managed certificates

1.  Create a global static IP for GCLB.

    ```bash
    gcloud compute addresses create apps-gclb-ip --global
    ```

1.  Get the static IP.

    ```bash
    echo -e "export APPS_GCLB_IP=$(gcloud compute addresses describe apps-gclb-ip --global --format=json | jq -r '.address')" >> $WORKDIR/vars.sh
    source $WORKDIR/vars.sh
    ```

1.  Create a free DNS name in the `cloud.goog` domain using Cloud Endpoints DNS service for Bank of Anthos.

    ```bash
    cat <<EOF > ${WORKDIR}/bank-openapi.yaml
    swagger: "2.0"
    info:
      description: "Cloud Endpoints DNS"
      title: "Cloud Endpoints DNS"
      version: "1.0.0"
    paths: {}
    host: "${BANK_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    x-google-endpoints:
    - name: "${BANK_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
      target: "${APPS_GCLB_IP}"
    EOF
    ```

1.  Create a free DNS name in the `cloud.goog` domain using Cloud Endpoints DNS service for Online Boutique.

    ```bash
    cat <<EOF > ${WORKDIR}/shop-openapi.yaml
    swagger: "2.0"
    info:
      description: "Cloud Endpoints DNS"
      title: "Cloud Endpoints DNS"
      version: "1.0.0"
    paths: {}
    host: "${SHOP_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    x-google-endpoints:
    - name: "${SHOP_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
      target: "${APPS_GCLB_IP}"
    EOF
    ```

1.  Deploy the openapi.yaml files.

    ```bash
    gcloud endpoints services deploy ${WORKDIR}/bank-openapi.yaml --async
    gcloud endpoints services deploy ${WORKDIR}/shop-openapi.yaml --async
    ```

1.  Create a Google managed cert for the domain.

    ```bash
    cat <<EOF > bank-managed-cert.yaml
    apiVersion: networking.gke.io/v1beta2
    kind: ManagedCertificate
    metadata:
      name: bank-managed-cert
      namespace: istio-system
    spec:
      domains:
        - "${BANK_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    EOF

    cat <<EOF > shop-managed-cert.yaml
    apiVersion: networking.gke.io/v1beta2
    kind: ManagedCertificate
    metadata:
      name: shop-managed-cert
      namespace: istio-system
    spec:
      domains:
        - "${SHOP_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    EOF

    kubectl --context ${CLUSTER_INGRESS} create namespace istio-system
    kubectl --context ${CLUSTER_INGRESS} apply -f bank-managed-cert.yaml
    kubectl --context ${CLUSTER_INGRESS} apply -f shop-managed-cert.yaml
    ```

1.  You can view the status of your certificates by descriving these resources. The `spec` and `status` fields show configured data.

    ```bash
    kubectl --context ${CLUSTER_INGRESS} -n istio-system describe managedcertificate bank-managed-cert
    kubectl --context ${CLUSTER_INGRESS} -n istio-system describe managedcertificate shop-managed-cert
    ```

The output is similar to the following:  

    Spec:
      Domains:
        shop.endpoints.asm-324577240955.cloud.goog
    Status:
      Certificate Name:    mcrt-c0458f29-bf40-434d-8e96-9efc4bcac9c6
      Certificate Status:  Provisioning

1.  Get the certificate resource names.

    ```bash
    echo -e "export BANK_MANAGED_CERT=$(kubectl --context ${CLUSTER_INGRESS} -n istio-system get managedcertificate bank-managed-cert -ojsonpath='{.status.certificateName}')" >> $WORKDIR/vars.sh
    echo -e "export SHOP_MANAGED_CERT=$(kubectl --context ${CLUSTER_INGRESS} -n istio-system get managedcertificate shop-managed-cert -ojsonpath='{.status.certificateName}')" >> $WORKDIR/vars.sh
    source $WORKDIR/vars.sh
    ```

> It can take up to 30 minutes for the certificate to provision. You can proceed with the following steps.  

1.  Create Cloud Armor policies. [Google Cloud Armor](https://cloud.google.com/armor) provides DDoS defense and [customizable security policies](https://cloud.google.com/armor/docs/configure-security-policies) that you can attach to a load balancer through Ingress resources. In the following steps, you create a security policy that uses [preconfigured rules](https://cloud.google.com/armor/docs/rule-tuning#preconfigured_rules) to block cross-site scripting (XSS) attacks.

    ```bash
    gcloud compute security-policies create gclb-fw-policy \
    --description "Block XSS attacks"

    gcloud compute security-policies rules create 1000 \
    --security-policy gclb-fw-policy \
    --expression "evaluatePreconfiguredExpr('xss-stable')" \
    --action "deny-403" \
    --description "XSS attack filtering"
    ```

## Configure Multicluster Ingress

1.  Create MCI and MCS

    ```bash
    cat <<EOF > ${WORKDIR}/apps-mci.yaml
    apiVersion: networking.gke.io/v1beta1
    kind: MultiClusterIngress
    metadata:
      name: apps-istio-ingressgateway-multicluster-ingress
      namespace: istio-system
      annotations:
        networking.gke.io/static-ip: "${APPS_GCLB_IP}"
        networking.gke.io/pre-shared-certs: "${BANK_MANAGED_CERT},${SHOP_MANAGED_CERT}"
    spec:
      template:
        spec:
          backend:
            serviceName: apps-istio-ingressgateway-multicluster-svc
            servicePort: 80
    EOF

    cat <<EOF > ${WORKDIR}/apps-mcs.yaml
    apiVersion: networking.gke.io/v1beta1
    kind: MultiClusterService
    metadata:
      name: apps-istio-ingressgateway-multicluster-svc
      namespace: istio-system
      annotations:
        beta.cloud.google.com/backend-config: '{"ports": {"80":"apps-backend-config"}}'
    spec:
      template:
        spec:
          selector:
            app: istio-ingressgateway
          ports:
          - name: frontend
            protocol: TCP
            port: 80 # Port the Service listens on
            targetPort: 8080 # Port the Endpoint or Pod listens on
      clusters:
      - link: "${CLUSTER_1_ZONE}/${CLUSTER_1}"
      - link: "${CLUSTER_2_ZONE}/${CLUSTER_2}"
    EOF

    cat <<EOF > ${WORKDIR}/apps-backend-config.yaml
    apiVersion: cloud.google.com/v1beta1
    kind: BackendConfig
    metadata:
      name: apps-backend-config
      namespace: istio-system
    spec:
      healthCheck:
        type: HTTP
        port: 15021
        requestPath: /healthz/ready
      securityPolicy:
        name: gclb-fw-policy
    EOF
    ```

1.  Apply the backendconfig, MCS and MCI manifests.

    ```bash
    kubectl --context ${CLUSTER_INGRESS} apply -f ${WORKDIR}/apps-backend-config.yaml

    kubectl --context ${CLUSTER_INGRESS} apply -f ${WORKDIR}/apps-mcs.yaml

    kubectl --context ${CLUSTER_INGRESS} apply -f ${WORKDIR}/apps-mci.yaml
    ```

1.  Wait until you get a GCLB IP and navigate to ensure Online Boutique works. CTRL-C to exit once you have an IP and navigate to the IP address in a browser.

    ```bash
    watch kubectl --context ${CLUSTER_INGRESS} -n istio-system get multiclusteringress -o jsonpath="{.items[].status.VIP}"
    ```

1.  Create a `Gateway` resource in the `istio-system` namespace. Creating this in the `istio-system` namespace allows you to use it in other namespaces (see VirtualService below). Gateways are usually owned by the platform admins or network admins team. Therefore, the Gateway resource is created in the `istio-system` namespace owned by the platform admin.

    ```bash
    cat <<EOF > ${WORKDIR}/apps-gateway.yaml
    apiVersion: networking.istio.io/v1alpha3
    kind: Gateway
    metadata:
      name: apps-gateway
      namespace: istio-system
    spec:
      selector:
        istio: ingressgateway # use Istio ingress gateway
      servers:
      - port:
          number: 80
          name: http
          protocol: HTTP
        hosts:
        - "${SHOP_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
        - "${BANK_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    EOF

    kubectl --context=${CLUSTER_1} apply -f ${WORKDIR}/apps-gateway.yaml
    kubectl --context=${CLUSTER_2} apply -f ${WORKDIR}/apps-gateway.yaml
    ```

1.  Create VirtualService in the bank and shop namespaces.

    ```bash
    cat <<EOF > ${WORKDIR}/bank-virtualservice.yaml
    apiVersion: networking.istio.io/v1alpha3
    kind: VirtualService
    metadata:
      name: bank-frontend-virtualservice
      namespace: bank-of-anthos
    spec:
      hosts:
      - "${BANK_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
      gateways:
      - istio-system/apps-gateway
      http:
        - route:
          - destination:
              host: frontend
              port:
                number: 80
    EOF

    kubectl --context=${CLUSTER_1} apply -f ${WORKDIR}/bank-virtualservice.yaml
    kubectl --context=${CLUSTER_2} apply -f ${WORKDIR}/bank-virtualservice.yaml

    cat <<EOF > ${WORKDIR}/shop-virtualservice.yaml
    apiVersion: networking.istio.io/v1alpha3
    kind: VirtualService
    metadata:
      name: shop-frontend-virtualservice
      namespace: online-boutique
    spec:
      hosts:
      - "${SHOP_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
      gateways:
      - istio-system/apps-gateway
      http:
        - route:
          - destination:
              host: frontend
              port:
                number: 80
    EOF

    kubectl --context=${CLUSTER_1} apply -f ${WORKDIR}/shop-virtualservice.yaml
    kubectl --context=${CLUSTER_2} apply -f ${WORKDIR}/shop-virtualservice.yaml
    ```

Note that the VirtualService is created in the application namespace. Typically, the application owner decides and conifgures how and what traffic gets routed to the application so VirtualService is deployed by the app owner. You can also deploy the VirtualServices centrally, for example, i the `istio-system` namespace (owned by the platform admin). Then you can use the `exportTo` value to share the VirtualService to the application namespace. This way you can cerntalize all Ingress control centrally. The decision depends upon your organization and roles and responsibility structure.  

## Access applications

You can access both `bank` and `shop` applications once the SSL certificates have been provisioned.  

1.  Check Bank of Anthos managed certificate status.

    ```bash
    kubectl --context ${CLUSTER_INGRESS} -n istio-system get managedcertificate bank-managed-cert -ojsonpath='{.status.certificateStatus}'
    kubectl --context ${CLUSTER_INGRESS} -n istio-system get managedcertificate shop-managed-cert -ojsonpath='{.status.certificateStatus}'
    ```

The output is similar to the following:  

    # Bank managed cert
    Active

    # Shop managed cert
    Active

It can take up to 30 minutes to provision Google managed certificates.  

1.  You can also check the certificate status via `gcloud`.

    ```bash
    gcloud beta compute ssl-certificates list
    ```

The output is similar to the following:  

    mcrt-bdc70848-db4c-4911-b1f2-e1b3832d496c  MANAGED  2021-03-20T13:38:44.407-07:00  2021-06-18T13:38:46.000-07:00  ACTIVE
        bank.endpoints.asm-multi-environment-cluster.cloud.goog: ACTIVE
    mcrt-f90cd39f-c706-4e51-9835-8493833ec2f8  MANAGED  2021-03-20T13:38:43.705-07:00  2021-06-18T12:53:51.000-07:00  ACTIVE
        shop.endpoints.asm-multi-environment-cluster.cloud.goog: ACTIVE

1.  You can access the applications via the Cloud Endpoints DNS name.

    ```bash
    echo -e "https://${BANK_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    echo -e "https://${SHOP_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    ```

Right after the managed certificates are provisioned, it may take a few moments for the site to become available. Navigate through both applications. They should be fully functional.  

## Securely exposing telemetry services using IAP

1.  Create a brand. The user issuing the request must be an owner of the specified support email address.

    ```bash
    gcloud alpha iap oauth-brands create --application_title=${PROJECT_ID}-app --support_email=${ADMIN_USER}
    ```

The output is similar to the following:  

    Created [49438368721].
    applicationTitle: asm-12345678-app
    name: projects/12345678/brands/12345678
    orgInternalOnly: true
    supportEmail: hello@exmaple.com

1.  Create an IAP Oauth client.

    ```bash
    gcloud alpha iap oauth-clients create projects/${PROJECT_ID}/brands/${PROJECT_NUM} --display_name=${PROJECT_ID}-app-oauth-client
    ```

The output is similar to the following:  

    Created [12345678-abvcdef.apps.googleusercontent.com].
    displayName: asm-12345678-app-oauth-client
    name: projects/12345678/brands/12345678/identityAwareProxyClients/12345678-abcdef.apps.googleusercontent.com
    secret: secretstring

1.  Get the CLIENT_ID and CLIENT_SECRET from the IAP Oauth client.

    ```bash
    export OAUTH_CLIENT_NAME=$(gcloud alpha iap oauth-clients list projects/$PROJECT_NUM/brands/$PROJECT_NUM --format="value(name)")
    echo -e "export CLIENT_ID=`echo ${OAUTH_CLIENT_NAME*/}`" >> $WORKDIR/vars.sh
    export CLIENT_SECRET=$(gcloud alpha iap oauth-clients list projects/$PROJECT_NUM/brands/$PROJECT_NUM --format="value(secret)")
    echo -e "export CLIENT_SECRET=${CLIENT_SECRET}" >> $WORKDIR/vars.sh
    source $WORKDIR/vars.sh
    ```

1.  Create a kubernetes secret with CLIENT_ID and CLIENT_SECRET from your OAuth Client.

    ```bash
    kubectl --context=${CLUSTER_INGRESS} create secret generic -n istio-system iap-oauth-client-secret \
    --from-literal=client_id=${CLIENT_ID} \
    --from-literal=client_secret=${CLIENT_SECRET}
    ```

1.  Reserve a static IP address for all telemetry components GCLB frontend.

    ```bash
    gcloud compute addresses create telemetry-gclb-ip --global
    ```

1.  Get the static IP.

    ```bash
    echo -e "export TELEMETRY_GCLB_IP=$(gcloud compute addresses describe telemetry-gclb-ip --global --format=json | jq -r '.address')" >> $WORKDIR/vars.sh
    source $WORKDIR/vars.sh
    ```

1.  Create a free DNS name in the `cloud.goog` domain using Cloud Endpoints DNS service for Kiali, Prometheus, Grafana and Jaeger.

    ```bash
    cat <<EOF > ${WORKDIR}/kiali-openapi.yaml
    swagger: "2.0"
    info:
      description: "Cloud Endpoints DNS"
      title: "Cloud Endpoints DNS"
      version: "1.0.0"
    paths: {}
    host: "${KIALI_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    x-google-endpoints:
    - name: "${KIALI_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
      target: "${TELEMETRY_GCLB_IP}"
    EOF

    cat <<EOF > ${WORKDIR}/prometheus-openapi.yaml
    swagger: "2.0"
    info:
      description: "Cloud Endpoints DNS"
      title: "Cloud Endpoints DNS"
      version: "1.0.0"
    paths: {}
    host: "${PROMETHEUS_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    x-google-endpoints:
    - name: "${PROMETHEUS_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
      target: "${TELEMETRY_GCLB_IP}"
    EOF

    cat <<EOF > ${WORKDIR}/grafana-openapi.yaml
    swagger: "2.0"
    info:
      description: "Cloud Endpoints DNS"
      title: "Cloud Endpoints DNS"
      version: "1.0.0"
    paths: {}
    host: "${GRAFANA_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    x-google-endpoints:
    - name: "${GRAFANA_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
      target: "${TELEMETRY_GCLB_IP}"
    EOF

    cat <<EOF > ${WORKDIR}/jaeger-openapi.yaml
    swagger: "2.0"
    info:
      description: "Cloud Endpoints DNS"
      title: "Cloud Endpoints DNS"
      version: "1.0.0"
    paths: {}
    host: "${JAEGER_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    x-google-endpoints:
    - name: "${JAEGER_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
      target: "${TELEMETRY_GCLB_IP}"
    EOF
    ```

1.  Deploy the openapi.yaml files.

    ```bash
    gcloud endpoints services deploy ${WORKDIR}/kiali-openapi.yaml --async
    gcloud endpoints services deploy ${WORKDIR}/prometheus-openapi.yaml --async
    gcloud endpoints services deploy ${WORKDIR}/grafana-openapi.yaml --async
    gcloud endpoints services deploy ${WORKDIR}/jaeger-openapi.yaml --async
    ```

1.  Create a Google managed cert for the four services.

    ```bash
    cat <<EOF > kiali-managed-cert.yaml
    apiVersion: networking.gke.io/v1beta2
    kind: ManagedCertificate
    metadata:
      name: kiali-managed-cert
      namespace: istio-system
    spec:
      domains:
        - "${KIALI_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    EOF

    cat <<EOF > prometheus-managed-cert.yaml
    apiVersion: networking.gke.io/v1beta2
    kind: ManagedCertificate
    metadata:
      name: prometheus-managed-cert
      namespace: istio-system
    spec:
      domains:
        - "${PROMETHEUS_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    EOF

    cat <<EOF > grafana-managed-cert.yaml
    apiVersion: networking.gke.io/v1beta2
    kind: ManagedCertificate
    metadata:
      name: grafana-managed-cert
      namespace: istio-system
    spec:
      domains:
        - "${GRAFANA_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    EOF

    cat <<EOF > jaeger-managed-cert.yaml
    apiVersion: networking.gke.io/v1beta2
    kind: ManagedCertificate
    metadata:
      name: jaeger-managed-cert
      namespace: istio-system
    spec:
      domains:
        - "${JAEGER_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    EOF

    kubectl --context ${CLUSTER_INGRESS} apply -f kiali-managed-cert.yaml
    kubectl --context ${CLUSTER_INGRESS} apply -f prometheus-managed-cert.yaml
    kubectl --context ${CLUSTER_INGRESS} apply -f grafana-managed-cert.yaml
    kubectl --context ${CLUSTER_INGRESS} apply -f jaeger-managed-cert.yaml
    ```

1.  You can view the status of your certificates by descriving these resources. The `spec` and `status` fields show configured data.

    ```bash
    kubectl --context ${CLUSTER_INGRESS} -n istio-system get managedcertificate kiali-managed-cert -ojsonpath='{.status.certificateName}'
    kubectl --context ${CLUSTER_INGRESS} -n istio-system get managedcertificate prometheus-managed-cert -ojsonpath='{.status.certificateName}'
    kubectl --context ${CLUSTER_INGRESS} -n istio-system get managedcertificate grafana-managed-cert -ojsonpath='{.status.certificateName}'
    kubectl --context ${CLUSTER_INGRESS} -n istio-system get managedcertificate jaeger-managed-cert -ojsonpath='{.status.certificateName}'
    ```

The output is similar to the following:  

    mcrt-b8c71e17-8cd5-40b9-aaba-08da0a330379
    mcrt-a7ae1c7d-c161-451e-bf5e-7e686a7b9cc8
    mcrt-4b935b5f-2efc-4910-8285-7ae6273a2031
    mcrt-9ab41440-9f22-4623-adb9-e12fcce71038

1.  Get the certificate resource names.

    ```bash
    echo -e "export KIALI_MANAGED_CERT=$(kubectl --context ${CLUSTER_INGRESS} -n istio-system get managedcertificate kiali-managed-cert -ojsonpath='{.status.certificateName}')" >> $WORKDIR/vars.sh
    echo -e "export PROMETHEUS_MANAGED_CERT=$(kubectl --context ${CLUSTER_INGRESS} -n istio-system get managedcertificate prometheus-managed-cert -ojsonpath='{.status.certificateName}')" >> $WORKDIR/vars.sh
    echo -e "export GRAFANA_MANAGED_CERT=$(kubectl --context ${CLUSTER_INGRESS} -n istio-system get managedcertificate grafana-managed-cert -ojsonpath='{.status.certificateName}')" >> $WORKDIR/vars.sh
    echo -e "export JAEGER_MANAGED_CERT=$(kubectl --context ${CLUSTER_INGRESS} -n istio-system get managedcertificate jaeger-managed-cert -ojsonpath='{.status.certificateName}')" >> $WORKDIR/vars.sh
    source $WORKDIR/vars.sh
    ```

1.  Create backend config.

    ```bash
    cat <<EOF > $WORKDIR/telemetry-backend-config.yaml
    apiVersion: cloud.google.com/v1beta1
    kind: BackendConfig
    metadata:
      name: telemetry-backend-config
      namespace: istio-system
    spec:
      healthCheck:
        checkIntervalSec: 2
        timeoutSec: 1
        healthyThreshold: 1
        unhealthyThreshold: 10
        port: 15021
        type: HTTP
        requestPath: /healthz/ready
      iap:
        enabled: true
        oauthclientCredentials:
          secretName: iap-oauth-client-secret
    EOF
    ```

1.  Create MCI and MCS resources.

    ```bash
    cat <<EOF > ${WORKDIR}/telemetry-mci.yaml
    apiVersion: networking.gke.io/v1beta1
    kind: MultiClusterIngress
    metadata:
      name: telemetry-istio-ingressgateway-multicluster-ingress
      namespace: istio-system
      annotations:
        networking.gke.io/static-ip: "${TELEMETRY_GCLB_IP}"
        networking.gke.io/pre-shared-certs: "${KIALI_MANAGED_CERT},${PROMETHEUS_MANAGED_CERT},${GRAFANA_MANAGED_CERT},${JAEGER_MANAGED_CERT}"
    spec:
      template:
        spec:
          backend:
            serviceName: telemetry-istio-ingressgateway-multicluster-svc
            servicePort: 80
    EOF

    cat <<EOF > ${WORKDIR}/telemetry-mcs.yaml
    apiVersion: networking.gke.io/v1beta1
    kind: MultiClusterService
    metadata:
      name: telemetry-istio-ingressgateway-multicluster-svc
      namespace: istio-system
      annotations:
        beta.cloud.google.com/backend-config: '{"ports": {"80":"telemetry-backend-config"}}'
    spec:
      template:
        spec:
          selector:
            app: istio-ingressgateway
          ports:
          - name: telemetry
            protocol: TCP
            port: 80 # Port the Service listens on
            targetPort: 8080 # Port the Endpoint or Pod listens on
      clusters:
      - link: "${CLUSTER_1_ZONE}/${CLUSTER_1}"
    EOF
    ```

1.  Apply the backendconfig, MCS and MCI manifests.

    ```bash
    kubectl --context ${CLUSTER_INGRESS} apply -f ${WORKDIR}/telemetry-backend-config.yaml

    kubectl --context ${CLUSTER_INGRESS} apply -f ${WORKDIR}/telemetry-mcs.yaml

    kubectl --context ${CLUSTER_INGRESS} apply -f ${WORKDIR}/telemetry-mci.yaml
    ```

1.  Wait until you get a GCLB IP and navigate to ensure Online Boutique works. CTRL-C to exit once you have an IP and navigate to the IP address in a browser.

    ```bash
    watch kubectl --context ${CLUSTER_INGRESS} -n istio-system get multiclusteringress telemetry-istio-ingressgateway-multicluster-ingress -o jsonpath="{.items[].status.VIP}"
    ```

1.  Create a Gateway and VirtualService for Kiali.

    ```bash
    cat <<EOF > ${WORKDIR}/telemetry-gateway.yaml
    apiVersion: networking.istio.io/v1alpha3
    kind: Gateway
    metadata:
      name: telemetry-gateway
      namespace: istio-system
    spec:
      selector:
        istio: ingressgateway # use Istio ingress gateway
      servers:
      - port:
          number: 80
          name: http
          protocol: HTTP
        hosts:
        - "${KIALI_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
        - "${PROMETHEUS_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
        - "${GRAFANA_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
        - "${JAEGER_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    EOF

    kubectl --context=${CLUSTER_1} apply -f ${WORKDIR}/telemetry-gateway.yaml

    cat <<EOF > ${WORKDIR}/kiali-virtualservice.yaml
    apiVersion: networking.istio.io/v1alpha3
    kind: VirtualService
    metadata:
      name: kiali-virtualservice
      namespace: monitoring
    spec:
      hosts:
      - "${KIALI_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
      gateways:
      - istio-system/telemetry-gateway
      http:
        - route:
          - destination:
              host: kiali
              port:
                number: 20001
    EOF

    cat <<EOF > ${WORKDIR}/prometheus-virtualservice.yaml
    apiVersion: networking.istio.io/v1alpha3
    kind: VirtualService
    metadata:
      name: prometheus-virtualservice
      namespace: monitoring
    spec:
      hosts:
      - "${PROMETHEUS_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
      gateways:
      - istio-system/telemetry-gateway
      http:
        - route:
          - destination:
              host: prometheus-1-prometheus
              port:
                number: 9090
    EOF

    cat <<EOF > ${WORKDIR}/grafana-virtualservice.yaml
    apiVersion: networking.istio.io/v1alpha3
    kind: VirtualService
    metadata:
      name: grafana-virtualservice
      namespace: monitoring
    spec:
      hosts:
      - "${GRAFANA_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
      gateways:
      - istio-system/telemetry-gateway
      http:
        - route:
          - destination:
              host: prometheus-1-grafana
              port:
                number: 80
    EOF

    cat <<EOF > ${WORKDIR}/jaeger-virtualservice.yaml
    apiVersion: networking.istio.io/v1alpha3
    kind: VirtualService
    metadata:
      name: jaeger-virtualservice
      namespace: monitoring
    spec:
      hosts:
      - "${JAEGER_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
      gateways:
      - istio-system/telemetry-gateway
      http:
        - route:
          - destination:
              host: jaeger-query
              port:
                number: 80
    EOF

    kubectl --context=${CLUSTER_1} apply -f ${WORKDIR}/kiali-virtualservice.yaml
    kubectl --context=${CLUSTER_1} apply -f ${WORKDIR}/prometheus-virtualservice.yaml
    kubectl --context=${CLUSTER_1} apply -f ${WORKDIR}/grafana-virtualservice.yaml
    kubectl --context=${CLUSTER_1} apply -f ${WORKDIR}/jaeger-virtualservice.yaml
    ```

1.  Add a user to the access policy for IAP.

    ```bash
    gcloud beta iap web add-iam-policy-binding \
    --member=user:${ADMIN_USER} \
    --role=roles/iap.httpsResourceAccessor
    ```

You can add more members and Google groups here to give other people on your team access to internal telemetry services.  

1.  Access Telemetry services.

    ```bash
    echo -e "https://${KIALI_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    echo -e "https://${PROMETHEUS_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    echo -e "https://${GRAFANA_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    echo -e "https://${JAEGER_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
    ```

> If you're trying to access Kiali from a user that is external to your Org, you need to set the orgInternalUsers setting to false in the Oauth consent screen. https://console.cloud.google.com/apis/credentials/consent. Ensure you are in the correct project. At this point, you cannot do this via command line so you can either use the Cloud COnsole or use the API directly from your terminal.  
> 
> Currently, there is a problem with Grafana being behind IAP. The login user token does not get propagated through login so you keep getting the login prompt even after login. Error message reads `Failed to look up user based on cookie" logger=context error="user token not found`.  

## Cleaning up

1.  Delete MCI. If you intend to delete your project, you do not need to do this step.

    ```bash
    kubectl --context ${CLUSTER_INGRESS} -n istio-system delete -f ${WORKDIR}/apps-mci.yaml
    kubectl --context ${CLUSTER_INGRESS} -n istio-system delete -f ${WORKDIR}/apps-mcs.yaml
    kubectl --context ${CLUSTER_INGRESS} -n istio-system delete -f ${WORKDIR}/apps-backend-config.yaml
    kubectl --context ${CLUSTER_INGRESS} -n istio-system delete -f ${WORKDIR}/telemetry-mci.yaml
    kubectl --context ${CLUSTER_INGRESS} -n istio-system delete -f ${WORKDIR}/telemetry-mcs.yaml
    kubectl --context ${CLUSTER_INGRESS} -n istio-system delete -f ${WORKDIR}/telemetry-backend-config.yaml
    ```

1.  Delete Cloud Endpoint Services and managed certificates.

    ```bash
    gcloud endpoints services delete ${BANK_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog --async --quiet
    gcloud endpoints services delete ${SHOP_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog --async --quiet
    gcloud endpoints services delete ${KIALI_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog --async --quiet
    gcloud endpoints services delete ${PROMETHEUS_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog --async --quiet
    gcloud endpoints services delete ${GRAFANA_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog --async --quiet
    gcloud endpoints services delete ${JAEGER_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog --async --quiet
    kubectl --context ${CLUSTER_INGRESS} -n istio-system delete -f ${WORKDIR}/bank-managed-cert.yaml
    kubectl --context ${CLUSTER_INGRESS} -n istio-system delete -f ${WORKDIR}/shop-managed-cert.yaml
    kubectl --context ${CLUSTER_INGRESS} -n istio-system delete -f ${WORKDIR}/kiali-managed-cert.yaml
    kubectl --context ${CLUSTER_INGRESS} -n istio-system delete -f ${WORKDIR}/prometheus-managed-cert.yaml
    kubectl --context ${CLUSTER_INGRESS} -n istio-system delete -f ${WORKDIR}/grafana-managed-cert.yaml
    kubectl --context ${CLUSTER_INGRESS} -n istio-system delete -f ${WORKDIR}/jaeger-managed-cert.yaml
    ```

1.  Verify that all endpoint services and managed certificates are deleted.

    ```bash
    gcloud endpoints services list
    gcloud compute ssl-certificates list
    ```

The output is similar to the following:  

    Listed 0 items.

1.  Delete Google Service Account created for Workload identity.

    ```bash
    gcloud iam service-accounts remove-iam-policy-binding \
    --role roles/iam.workloadIdentityUser \
    --member "serviceAccount:${PROJECT_ID}.svc.id.goog[bank-of-anthos/${KSA_NAME}]" \
    ${GSA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com
    gcloud iam service-accounts delete ${GSA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com --quiet
    ```

1.  Delete hub registration.

    ```bash
    gcloud container hub memberships delete ${CLUSTER_INGRESS} --quiet
    gcloud container hub memberships delete ${CLUSTER_1} --quiet
    gcloud container hub memberships delete ${CLUSTER_2} --quiet
    ```

1.  Delete the clusters.

    ```bash
    gcloud container clusters delete ${CLUSTER_INGRESS} --zone ${CLUSTER_INGRESS_ZONE} --async --quiet
    gcloud container clusters delete ${CLUSTER_1} --zone ${CLUSTER_1_ZONE} --async --quiet
    gcloud container clusters delete ${CLUSTER_2} --zone ${CLUSTER_2_ZONE} --async --quiet
    ```

1.  Delete the GCLB IPs.

    ```bash
    gcloud compute addresses delete apps-gclb-ip --global --quiet
    gcloud compute addresses delete telemetry-gclb-ip --global --quiet
    ```

1.  Delete Cloud Armor Security policy.

    ```bash
    gcloud compute security-policies delete gclb-fw-policy --quiet
    ```

1.  Confirm endpoints, certificates, hub registration and clusters are deleted.

    ```bash
    gcloud endpoints services list
    gcloud compute security-policies list
    gcloud compute ssl-certificates list
    gcloud container hub memberships list
    gcloud container clusters list
    ```

The output is similar to the following:  

    Listed 0 items.

1.  Delete project and folder.

    ```bash
    gcloud projects delete $PROJECT_ID --quiet
    gcloud resource-manager folders delete $FOLDER_ID --quiet
    ```

1.  Unset KUBECONFIG and WORKDIR

    ```bash
    unset KUBECONFIG
    cd $HOME && rm -rf $WORKDIR
    ```

    
